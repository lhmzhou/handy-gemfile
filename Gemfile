source 'https://rubygems.org'

ruby '3.1.0'

gem 'rails', github: 'rails/rails' # bundle edge rails instead
gem 'tzinfo-data', '>= 1.2016.7'  # Don't rely on OSX/Linux timezone data

# Action Text
gem 'actiontext', github: 'basecamp/actiontext', ref: 'okra'
gem 'okra', github: 'basecamp/okra'

# Drivers
gem 'mysql2'
gem 'sqlite3'  # used sqlite3 as the database for active record
gem 'redis', '~> 4.0'
gem 'redis_connectable', github: 'basecamp/redis_connectable'
gem 'redis-namespace'
gem 'connection_pool'
gem 'image_processing', '~> 1.2'

gem 'puma', '>= 4.3.3', github: 'puma/puma'

# JavaScript and assets
gem 'webpacker', '~> 5.1.1'
gem 'sprockets', github: 'rails/sprockets'
gem 'sprockets-rails', github: 'rails/sprockets-rails'
gem 'jbuilder', '~> 2.9', '>= 2.9.1', github: 'rails/jbuilder'
gem 'sassc-rails', '~> 2.1'
gem 'sassc', '<= 2.1'
gem 'local_time', '~> 2.0'
gem 'turbo', github: 'basecamp/turbo'

# Security
gem 'bcrypt', '~> 3.1.7'
gem 'pwned', '~> 2.0'
gem 'rotp'
gem 'webauthn'
gem 'rack-ratelimit', github: 'jeremy/rack-ratelimit'

# Jobs
gem 'resque', '~> 2.0.0'
gem 'resque-multi-job-forks', '~> 0.5'
gem 'resque-pool', github: 'nevans/resque-pool'
gem 'resque-scheduler', github: 'resque/resque-scheduler'
gem 'resque-pause', github: 'basecamp/resque-pause'
gem 'resque-web', require: 'resque_web'
gem 'resque-scheduler-web', github: 'mattgibson/resque-scheduler-web'
gem 'sinatra', github: 'sinatra/sinatra'

# Storage
gem 'aws-sdk-s3', '~> 1.48.0'
gem 'activestorage-redundancy', github: 'basecamp/activestorage-redundancy'
gem 'activestorage-office-previewer', '~> 0.1'

# Search
gem 'elasticsearch-model', github: 'elastic/elasticsearch-rails', branch: '6.x'
gem 'elasticsearch-rails', github: 'elastic/elasticsearch-rails', branch: '6.x'
gem 'html_scrubber', github: 'basecamp/html_scrubber'

# Push notifications
gem 'aws-sdk-pinpoint'

# Monitoring
gem 'easymon', '~> 1.4.2'
gem 'sentry-raven'

# Logging/instrumentation
gem 'rails_structured_logging', github: 'basecamp/rails-structured-logging'
gem 'full_request_logger', github: 'basecamp/full_request_logger'
gem 'critter', '~> 0.3.0', github: 'basecamp/critter', require: false
gem 'activejob-stats', github: 'basecamp/activejob-stats'

# Profiling/troubleshooting
gem 'rbtrace'
gem 'stackprof'
gem 'rack-mini-profiler'
gem 'flamegraph'

# Queenbee
gem 'queenbee', github: 'basecamp/queenbee-plugin'
gem 'actionpack-xml_parser'
gem 'countries'

# vCard parsing
gem 'pdi', github: 'basecamp/pdi', branch: 'utf-8-default'

# Supervised rails console in production environments
gem 'console1984', github: 'basecamp/console1984'

# Others
gem 'rake', github: 'ruby/rake'
gem 'platform_agent'
gem 'useragent', github: 'basecamp/useragent'
gem 'geared_pagination', '>= 1.0.0'
gem 'rqrcode'
gem 'base32'
gem 'httparty'
gem 'bootsnap', '>= 1.4.2', require: false # Reduces boot times through caching; required in config/boot.rb
gem 'rspamd', github: 'basecamp/rspamd-ruby'
gem 'redis_object', github: 'basecamp/redis_object'
gem 'ruby-progressbar', require: false
gem 'rinku', require: 'rails_rinku'
gem 'active_record_encryption', github: 'basecamp/active_record_encryption'
gem 'simpleidn'
gem 'uglifier', '>= 1.3.0' # use uglifier as compressor for javascript assets
gem 'coffee-rails' # coffeescript for .coffee assets and view
gem 'therubyracer', platforms: : ruby
gem 'carrierwave' # handle file uploads
gem 'mini-magick', '~> 3.5.0' # fun different filters, and file resizing
gem 'fog'
gem 'figaro' # credentials are not passed up to respository

group :development, :test do
  gem 'byebug'
  gem 'break'
  gem 'spring'

  # Code critics
  gem 'rubocop', '>= 0.72', require: false
  gem 'rubocop-performance', require: false
  gem 'rubocop-rails', require: false
  gem 'scss_lint', '~> 0.50', require: false
  gem 'bundler-audit', '~> 0.4', github: 'basecamp/bundler-audit', branch: 'thor-bump', require: false
  gem 'brakeman', '>= 4.0', require: false
  gem 'benchmark-ips', require: false
end

group :development do
  gem 'web-console'
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'faker', require: false

  # Deploys, local CI
  gem 'haybales', github: 'basecamp/haybales'
end

group :test do
  gem 'mocha'
  gem 'capybara', '>= 2.15', github: 'teamcapybara/capybara'
  gem 'selenium-webdriver'
  gem 'webmock', github: 'bblimke/webmock'
  gem 'vcr'
end
